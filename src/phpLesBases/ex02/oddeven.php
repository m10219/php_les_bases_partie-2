<?php

const TYPE_NUMBER = 'Entrez un nombre: ';

while (true) {
    echo $output ?? TYPE_NUMBER;

    $number = trim(fgets(STDIN));
    $answer = is_numeric($number)
        ? "Le chiffre $number est " . ($number % 2 ? 'Impair' : 'Pair') . "\n"
        : "'$number'" . " n'est pas un chiffre\n";
    $output = $answer . TYPE_NUMBER;
}
