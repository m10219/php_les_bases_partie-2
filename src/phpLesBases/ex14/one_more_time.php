<?php

if ($argc != 2) {
    exit;
} elseif ($argv[1] == null) {
    exit;
}
$regexdate = '/^(?i)(?<jour>Lundi|Mardi|mercredi|jeudi|vendredi|samedi|dimanche) (?<datenum>3[01]|[12][0-9]|0?[1-9]) (?<mois>janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre) (?<annee>\d{1,4})(?:\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])$/'; // valide le bon format de la date, heure etc...
if (!preg_match_all($regexdate, $argv[1], $arr1, PREG_SET_ORDER)) {
    echo "Wrong Format\n";
    exit;
}
$arrmois = ['janvier' => 1, 'février' => 2, 'mars' => 3, 'avril' => 4, 'mai' => 5, 'juin' => 6, 'juillet' => 7, 'août' => 8, 'septembre' => 9, 'octobre' => 10, 'novembre' => 11, 'décembre' => 12];
foreach ($arrmois as $mois => $nummois) {
    if (!strcasecmp($arr1[0]['mois'], $mois)) {        // comparaison de string insensible à la casse, return 0 si pareil, 1 si différent
        if (!checkdate($nummois, $arr1[0]['datenum'], $arr1[0]['annee'])) {
            echo "Wrong Format\n";
            exit;
        }
        $datetime = $argv[1] . ' heure normale d’Europe centrale';
        $parser = new IntlDateFormatter(
                'fr_FR',
                IntlDateFormatter::FULL,
                IntlDateFormatter::FULL
            );
        $tsparis = $parser->parse($datetime); // transforme la date en seconde depuis le 01/01/1970
        echo $tsparis . "\n";
        exit;
    }
}

// if ( strcasecmp ($arr1[0]['mois'], 'janvier') == 0 ) {
//     $mois = "janvier";
//     $nummois = "1";
// } else if ( strcasecmp ($arr1[0]['mois'], 'février') == 0 ) {
//     $mois = "février";
//     $nummois = "2";
// } else if ( strcasecmp ($arr1[0]['mois'], 'mars') == 0 ) {
//     $mois = "mars";
//     $nummois = "3";
// } else if ( strcasecmp ($arr1[0]['mois'], 'avril') == 0 ) {
//     $mois = "avril";
//     $nummois = "4";
// } else if ( strcasecmp ($arr1[0]['mois'], 'mai') == 0 ) {
//     $mois = "mai";
//     $nummois = "5";
// } else if ( strcasecmp ($arr1[0]['mois'], 'juin') == 0 ) {
//     $mois = "juin";
//     $nummois = "6";
// } else if ( strcasecmp ($arr1[0]['mois'], 'juillet') == 0 ) {
//     $mois = "juillet";
//     $nummois = "7";
// } else if ( strcasecmp ($arr1[0]['mois'], 'août') == 0 ) {
//     $mois = "aout";
//     $nummois = "8";
// } else if ( strcasecmp ($arr1[0]['mois'], 'septembre') == 0 ) {
//     $mois = "septembre";
//     $nummois = "9";
// } else if ( strcasecmp ($arr1[0]['mois'], 'octobre') == 0 ) {
//     $mois = "octobre";
//     $nummois = "10";
// } else if ( strcasecmp ($arr1[0]['mois'], 'novembre') == 0 ) {
//     $mois = "novembre";
//     $nummois = "11";
// } else if ( strcasecmp ($arr1[0]['mois'], 'décembre') == 0 ) {
//     $mois = "décembre";
// //     $nummois = "12";
// // }
// if (!checkdate($nummois, $arr1[0]['datenum'], $arr1[0]['annee'])) {
//     echo "Wrong Format\n";
// }
// $datetime = $argv[1]." heure normale d’Europe centrale";
// $parser = new IntlDateFormatter(
//         'fr_FR',
//         IntlDateFormatter::FULL,
//         IntlDateFormatter::FULL
//     );
//     $tsparis = $parser->parse($datetime);
//     echo $tsparis."\n";
