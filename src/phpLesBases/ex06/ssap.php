<?php

// transforme argv en chaine de characteres separés par un espace à partie de la clé 1
    $strg = implode(' ', array_slice($argv, 1));

// reforme un tableau avec valeur = chaque chaine de caractere separée par des espaces blanc puis trie les valeurs par ordre alphabétiques case sensitive
    $tab = preg_split("/\s/", $strg, -1, PREG_SPLIT_NO_EMPTY);
    sort($tab);

// affiche chaque valeur du tableau avec un retour ligne à chaque fois
    foreach ($tab as $key => $value) {
        echo "$value\n";
    }
