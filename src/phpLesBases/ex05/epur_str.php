<?php

if ($argc == 2) {
    $clean = preg_replace('#\s+#', ' ', $argv[1], -1);
    $remove_head_tail = trim($clean);

    if ($remove_head_tail != null) {
        echo "$remove_head_tail\n";
    }
}

// && !empty($argv [1])
// $arg1 = preg_replace('/\s+/', ' ', $mystring);
// $remove_last = rtrim($arg1, " ");
// $remove_start = ltrim($remove_last, " ");

// echo("This is the string after removal:\n". "$arg1")."/";

/*
ligne 4 et 5 : 
$remove_head_tail = preg_replace("/\s+/", ' ', trim($remove_head_tail));
*/