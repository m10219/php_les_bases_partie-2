<?php

// "https://localhost/phpLesBases/ex18/cookie_crisp.php?action=set&name=plat&value=choucroute"
// "https://localhost/phpLesBases/ex18/cookie_crisp.php?action=get&name=plat"
// "https://localhost/phpLesBases/ex18/cookie_crisp.php?action=del&name=plat"

if ($_GET['action'] == 'set') {
    if ($_GET['name'] && $_GET['value']) {
        setcookie($_GET['name'], $_GET['value'], time() + 3600);
    }
}

if ($_GET['action'] == 'get' && setcookie($_GET['name'])) {
    if ($value = $_COOKIE[$_GET['name']]) {
        echo $value . "\n";
    }
}
if ($_GET['action'] == 'del') {
    setcookie($_GET['name'], '', time() - 3600);
}
